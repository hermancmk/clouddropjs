/*
 * @author          Herman Karlsson <hermancmk@gmail.com>
 * @copyright       2014 Herman Karlsson
 * @license         {@link https://github.com/hermancmk/CloudDropJS/blob/master/LICENSE | MIT Licensse}
*/

//Global namespace
var CloudDrop = CloudDrop || {
    //Context variable; needed in most files
    context: null
};