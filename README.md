CloudDropJS
===========

CloudDropJS is an easy to use game engine, built to be small and fast to use.  
CloudDropJS is updated almost daily with new bug fixes and features. If you find any bugs or want to request a new feature, open a new issue on github or email me at hermancmk@gmail.com.

##Documentation
[CloudDrop.js](http://htmlpreview.github.io/?https://github.com/hermancmk/CloudDropJS/blob/master/docs/CloudDrop.html)  
[Context.js](http://htmlpreview.github.io/?https://github.com/hermancmk/CloudDropJS/blob/master/docs/Context.html)

##Changelog
#####Version 0.1.0
* Added context file + Method
* Added optimized main loop
