/*
 * @author          Herman Karlsson <hermancmk@gmail.com>
 * @copyright       2014 Herman Karlsson
 * @license         {@link https://github.com/hermancmk/CloudDropJS/blob/master/LICENSE | MIT Licensse}
*/

//Global namespace
var CloudDrop = CloudDrop || {
    //Context variable; needed in most files
    context: null
};

//Creates a canvas and gets it's context.
//This context is what everything will be rendered on
//Arguments
//---
//Width - Integer: The width of the renderering context
//
//Height - Integer: The height of the renderering context
//
//Update - Function reference: A function to be updated every frame
CloudDrop.Context = function(width, height, update) {
    this.width = width;
    this.height = height;
    this.update = update;
    this.canvas = null;
    this.mainLoop = null;
    this.animFrame = null,
    this.recursiveAnim = null,
    
    //The canvas element is created with z-index 1 and a 1px border around it
    //This can easely be changed with css if un-wanted
    canvas = document.createElement('canvas');

    canvas.id = 'canvas';
    canvas.width = width;
    canvas.height = height;
    canvas.style.zIndex = 1;
    canvas.style.position = 'absolute';
    canvas.style.border = '1px solid';

    var body = document.getElementsByTagName('body')[0];
    body.appendChild(canvas);

    CloudDrop.context = canvas.getContext('2d');
    
    //The main loop is optimized and has fallback to an un-optimized version
    //It runs in 60 frame per second top
    mainloop = function() {
        update();
    };

    animFrame = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        null ;

    if (animFrame !== null) {
        recursiveAnim = function() {
            mainloop();
            animFrame(recursiveAnim);
        };

        animFrame(recursiveAnim);
    } else {
        setInterval(mainloop, 1000 / 60);
    }
}